variable "profile" {
  description = "Nome do profile da AWS. Geralmente encontrado em ~/.aws/credentials"
  default = "default"
}

variable "region" {
  description = "Região do remote backend"
  default = "sa-east-1"
}

variable "bucket_remote" {
  description = "Nome do bucket que armazenará os dados do state do backend"
}

variable "dynamodb_remote" {
  description = "Nome da tabela que armazenará o lock do state do backend"
}
